<?php

add_action('acf/init', 'jj_acf_blocks');

function jj_acf_blocks() {
    // Check function exists.
    if( function_exists('acf_register_block') ) {

        acf_register_block(array(
            'name'              => 'homepage-slider',
            'title'             => __('Homepage Slider'),
            'description'       => __('A custom slider block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'          => 'design',
            'icon'              => 'slides',
            'keywords'          => array( 'slider'),
            'mode'              => 'auto'
        ));

        acf_register_block(array(
            'name'              => 'three-column-grid',
            'title'             => __('Three Column Grid'),
            'description'       => __('A custom grid block.'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'          => 'design',
            'icon'              => 'grid-view',
            'keywords'          => array( 'slider'),
            'mode'              => 'auto'
        ));

        acf_register_block(array(
            'name'              => 'who-we-are',
            'title'             => __('Who We Are Block'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'          => 'design',
            'icon'              => 'star-filled',
            'keywords'          => array( 'company'),
            'mode'              => 'auto'
        ));

        acf_register_block(array(
            'name'              => 'scroll-pin',
            'title'             => __('Scroll Pin Block'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'          => 'design',
            'icon'              => 'align-full-width',
            'keywords'          => array( 'scroll'),
            'mode'              => 'auto'
        ));
        acf_register_block(array(
            'name'            => 'testimonial',
            'title'           => __( 'Testimonial', 'junkjockey' ),
            'render_callback' => 'my_acf_block_render_callback',
            'category'        => 'design',
            'icon'            => 'testimonial',
            'keywords'        => array( 'testimonial, review' ),
            'mode'            => 'auto',
        ));
        acf_register_block(
			array(
				'name'            => 'logos',
				'title'           => __( 'Logos', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'format-image',
				'category'        => 'design',
				'keywords'        => array( 'logo' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'text-block',
				'title'           => __( 'Text Block', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'editor-alignleft',
				'category'        => 'design',
				'keywords'        => array( 'Text' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'gallery-block',
				'title'           => __( 'Gallery Slider', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'slides',
				'category'        => 'design',
				'keywords'        => array( 'Gallery' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'text-cta',
				'title'           => __( 'Text Baner CTA', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'align-wide',
				'category'        => 'design',
				'keywords'        => array( 'Text CTA' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'video-block',
				'title'           => __( 'Video Block', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'video-alt3',
				'category'        => 'design',
				'keywords'        => array( 'video' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'profile-block',
				'title'           => __( 'Profile Block', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'admin-users',
				'category'        => 'design',
				'keywords'        => array( 'video' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'featured-page-block',
				'title'           => __( 'Featured Page Block', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'admin-page',
				'category'        => 'design',
				'keywords'        => array( 'video' ),
				'mode'            => 'auto',
			)
		);
        acf_register_block(
			array(
				'name'            => 'faq-block',
				'title'           => __( 'FAQs Block', 'junkjockey' ),
                'render_callback' => 'my_acf_block_render_callback',
				'icon'            => 'editor-ul',
				'category'        => 'design',
				'keywords'        => array( 'video' ),
				'mode'            => 'auto',
			)
		);
    }
}

function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/{$slug}.php") );
	}
}

add_filter('allowed_block_types_all', 'jj_allowed_block_types', 10, 2);
function jj_allowed_block_Types($allowed_blocks) {
    $allowed_blocks = array(
        'core/block',
        'acf/homepage-slider',
        'acf/three-column-grid',
        'acf/who-we-are',
        'acf/scroll-pin',
        'acf/text-block',
        'acf/gallery-block',
        'acf/text-cta',
        'acf/video-block',
        'acf/profile-block',
        'acf/featured-page-block',
        'acf/faq-block'
    );
    return $allowed_blocks;
}