<?php

require_once('src/Controller/GutenbergController.php');

wp_enqueue_style('themestyle', get_template_directory_uri() . '/dist/app.css', false, 1.0, 'all');

if(!is_admin()) {
    wp_enqueue_script('themescript', get_template_directory_uri() . '/dist/bundle.js', '', 1.0, true );   
}

// This theme uses wp_nav_menu() in one location.
register_nav_menus(
    array(
        'menu-1' => esc_html__( 'Primary', 'junkjockey' ),
        'menu-2' => esc_html__( 'Footer', 'junkjockey' ),
        'menu-3' => esc_html__( 'Social', 'junkjockey' ),
    )
);

//ADD IMAGE SIZE
add_image_size('fullwidth-retina', 4000, 2800); // based on 12 columns
add_image_size('fullwidth', 2000, 1400); // based on 12 columns
add_image_size('square-large-retina', 2000, 2000, true);
add_image_size('square-large', 1000, 1000, true);
add_image_size('square', 500, 500, true);
add_image_size('thumbnail', 150, 150);

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => 'Sitewide Details',
        'menu_title'    => 'Sitewide Details',
        'menu_slug'     => 'sitewide-details',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
}