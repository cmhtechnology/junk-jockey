import Swiper, { Navigation, Pagination, EffectFade } from 'swiper';
Swiper.use([Navigation,Pagination,EffectFade]);

const homepageSlider = (target) => {
    new Swiper(target, {
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        slidesPerView: 1,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    })
}

const testimonialSlider = (target) => {
    new Swiper(target, {
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
    })
}

const gallerySlider = (target) => {
    new Swiper(target, {
        slidesPerView: 1.5,
        spaceBetween: 20,
        breakpoints: {
            768: {
                slidesPerView: 'auto',
                spaceBetween: 20
            }
        }
    })
}

export {homepageSlider, testimonialSlider, gallerySlider}