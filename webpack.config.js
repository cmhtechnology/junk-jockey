const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var path = require('path');

// change these variables to fit your project
const jsPath= './assets/js';
const cssPath = './assets/sass';
const outputPath = 'dist';
const imagePath = 'images';
const fontPath = 'fonts'
const localDomain = 'http://localhost/junk-jockey';
const entryPoints = {
  // 'app' is the output name, people commonly use 'bundle'
  // you can have more than 1 entry point
  'bundle': jsPath + '/app.js',
  'app': cssPath + '/app.scss',
  'gutenberg' : cssPath + '/gutenberg.scss'
};

module.exports = {
  entry: entryPoints,
  output: {
    path: path.resolve(__dirname, outputPath),
    filename: '[name].js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),

    // Uncomment this if you want to use CSS Live reload
    /*
    new BrowserSyncPlugin({
      proxy: localDomain,
      files: [ outputPath + '/*.css' ],
      injectCss: true,
    }, { reload: false, }),
    */
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node-modules/,
        use: 'babel-loader',
      } 
    ],
    rules: [
      {
        test: /\.s?[c]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.sass$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'url-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: { indentedSyntax: true },
              sourceMap: true,
            }
            
          }
        ]
      },
      {
        test: /\.(jpg|jpeg|png|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          publicPath: imagePath,
          outputPath: '/images',
          name: '[name].[ext]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/i,
        loader: 'file-loader',
        options: {
          publicPath: fontPath,
          outputPath: '/fonts',
          name: '[name].[ext]'
        }
      }
    ]
  },
  devtool: 'inline-source-map'
};