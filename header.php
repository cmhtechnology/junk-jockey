<!DOCTYPE html>
<html lang="en" class="is-loading">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicon and Apple Icons -->
    <link rel="shortcut icon" href="<?php echo $favicon; ?>">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $touch_icon; ?>">

    <?php wp_head();  ?>
</head>
<body <?php body_class(); ?>>

<header class="header">
    <div class="top-header">
        <div class="container no-gutter">
            <div class="left">
                <div class="address">
                    <?php $address = get_field('address', 'option');
                        if($address) {
                            echo $address;
                        }
                    ?>
                </div>
                <div class="phone">
                    <?php $contact_phone_number = get_field('contact_phone_number', 'option');
                        if($contact_phone_number) {
                            echo '<a href="tel:'.$contact_phone_number.'">'.$contact_phone_number.'</a>';
                        }
                    ?>
                </div>
            </div>
            <div class="right">
                <div class="email">
                    <?php $contact_email = get_field('contact_email', 'option');
                        if($contact_email) {
                            echo '<a href="mailto:'.$contact_email.'">'.$contact_email.'</a>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-header">
        <div class="container no-gutter">
            <div class="logo">
                <?php $site_logo = get_field('site_logo_header', 'option');
                    if($site_logo):?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <img src="<?php echo $site_logo['url']; ?>" alt="<?php bloginfo('name'); ?> logo"/>
                    </a>
                <?php endif;?>
            </div>
            <div class="bottom-right-header">
                <?php $header_button = get_field('header_button', 'option');
                    if($header_button):
                        $link_url = $header_button['url'];
                        $link_title = $header_button['title'];
                        $link_target = $header_button['target'] ? $header_button['target'] : '_self';
                ?>
                    <a class="btn btn-rounded btn-red" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif;?>

                <div class="phone-icon">
                    <?php $contact_phone_number = get_field('contact_phone_number', 'option');
                        if($contact_phone_number) {
                            echo '<a href="tel:'.$contact_phone_number.'"></a>';
                        }
                    ?>
                </div>

                <div class="menu-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>

<?php if(!is_front_page()):?>
    <?php get_template_part( 'template-parts/banner-page' ); ?>
<?php endif;?>

<main>