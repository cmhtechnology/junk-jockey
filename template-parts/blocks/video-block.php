<?php $video_cover = get_field('image_cover');
if(!empty($video_cover) && is_array($video_cover)):?>
<section class="gutenberg-block gutenberg-video-block">
    <div class="container">
        <div class="video-container bg-cover lazyload"
             data-bgset="
                <?php echo $video_cover['sizes']['fullwidth']; ?>.webp 1x, <?php echo $video_cover['sizes']['fullwidth-retina']; ?>.webp 2x
                "
        >
        <a data-fancybox="video-gallery" data-width="1920" data-height="1000" data-src="<?php echo get_field('youtube_link');?>">
        </a>
        </div>
    </div>
</section>
<?php endif;?>