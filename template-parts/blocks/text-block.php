<?php
$layout_type = get_field('select_layout');
?>
<section class="gutenberg-block gutenberg-text-content-block">
    <div class="container">
        <?php if($layout_type === "column"):?>
            <?php
                $two_column = get_field("two_column");
            ?>
            <div class="two-column-container">
                <div class="heading">
                    <h2><?php echo $two_column['2_column_heading'];?></h2>
                </div>
                <div class="content">
                    <div class="left">
                        <?php echo $two_column['left'];?>
                    </div>
                    <div class="right">
                        <?php echo $two_column['right'];?>
                    </div>
                </div>
            </div>
        <?php elseif ($layout_type === "sideheading"):?>
            <?php $left_heading = get_field('left_heading');?>
            <div class="side-header-container">
                <div class="heading">
                    <h2><?php echo $left_heading['left_heading'];?></h2>
                </div>
                <div class="content">
                    <?php echo $left_heading['right_content'];?>
                </div>
            </div>
        <?php endif;?>
            
    </div>
</section>