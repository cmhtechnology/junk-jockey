<?php 
$pages = get_field('select_pages');
$extra_page = get_field('pricing_page');
$column_count = get_field('column_count');
$bg_color = get_field('background_color');

if($column_count === 'onec') {
    $column = 'col-12';
} elseif($column_count === 'twoc') {
    $column = 'col-6';
} elseif($column_count === 'threec') {
    $column = 'col-4 col-md-down-6';
}

if(!empty($pages) && is_array($pages)):?>
<section class="gutenberg-block gutenberg-featured-page-block">
    <div class="featured-page-container row">
        <?php foreach($pages as $page):
            $fields = get_fields($page->ID);
            $title = get_the_title($page->ID);
            $permalink = get_the_permalink($page->ID);
            ?>
            <div class="<?php echo $column;?>">
                <div class="featured-page">
                    <div class="page-image">
                    <?php if(is_admin()):?>
                        <img src="<?php echo $fields['banner_image']['url'];?>">
                    <?php else:?>
                        <a href="<?php echo $permalink;?>"></a>
                            <picture>
                                <source
                                        data-srcset="<?php echo $fields['banner_image']['sizes']['fullwidth']; ?>.webp 1x, <?php echo $fields['banner_image']['sizes']['fullwidth-retina']; ?>.webp 2x" type="img/webp"
                                />
                                <source
                                    data-srcset="<?php echo $fields['banner_image']['sizes']['fullwidth']; ?> 1x, <?php echo $fields['banner_image']['sizes']['fullwidth-retina']; ?> 2x" type="<?php echo $fields['banner_image']['mime_type']; ?>"
                                />
                                <img
                                    class="lazyload"
                                    data-src="<?php echo $fields['banner_image']['sizes']['fullwidth']; ?>"
                                    alt="<?php echo $fields['banner_image']['title']; ?>">
                            </picture>
                    <?php endif;?>
                    </div>
                    <div class="title">
                        <?php echo $title;?>
                        <a class="btn btn-circle-arrow btn-circle-arrow__border-red" href="<?php echo $permalink;?>">Learn More</a>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
        <?php if($extra_page):?>
            <div class="<?php echo $column;?>">
                <div class="featured-page extra-page <?php echo $bg_color ? 'red' : 'blue';?>">
                <a href="<?php echo $permalink;?>"></a>
                <div class="title">
                        <?php echo get_the_title($extra_page->ID);?>
                        <a class="btn btn-circle-arrow btn-circle-arrow__border-white" href="<?php echo get_the_permalink($extra_page->ID);?>">Learn More</a>
                    </div>
                </div>
            </div>
        <?php endif;?>
    </div>
</section>
<?php endif;?>