<?php
$grid_1 = get_field('column_1');
$grid_2 = get_field('column_2');
$grid_3 = get_field('column_3');

// echo '<pre>';
// echo var_dump($grid_1['image']);
// echo '</pre>';
?>
<div class="gutenberg-block gutenberg-three-column-grid">
    <div class="container">
        <div class="three-column-grid-container">
            <?php if(!empty($grid_1) && is_array($grid_1)):?>
                <div class="column-grid">
                    <?php if(is_admin()):?>
                        <img src="<?php echo $grid_1['image']['url'];?>"/>
                    <?php else:?>
                        <picture>
                            <source
                                data-srcset="<?php echo $grid_1['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_1['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:400px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_1['image']['sizes']['square-large']; ?>.webp 1x, <?php echo $grid_1['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:768px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_1['image']['sizes']['square-large-retina']; ?>.webp 1x, <?php echo $grid_1['image']['sizes']['square']; ?>.webp 2x"
                                media="(max-width:5000px)"
                            >
                            <img
                                class="lazyload"
                                data-src="<?php echo $grid_1['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_1['image']['sizes']['square-large-retina']; ?>.webp 2x"
                                alt="<?php echo $grid_1['image']['title']; ?>">
                        </picture>
                    <?php endif;?>
                    <div class="col-grid-content">
                        <div class="col-title">
                            <?php echo $grid_1['title'];?>
                        </div>
                        <div class="short-desc">
                            <?php echo $grid_1['short_description'];?>
                        </div>
                        <?php $link = $grid_1['link'];
                        if($link):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="btn btn-link-arrow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php echo esc_html( $link_title ); ?>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="15" viewBox="0 0 18.5 15"><path id="Union_1" data-name="Union 1" d="M-6775.5-1444h-8.5v-3h8.5v-6l10,7.5-10,7.5Z" transform="translate(6784 1453)" fill="#EE2628"/></svg>
                                </span>
                            </a>
                        <?php endif;?>
                    </div>
                </div>
            <?php endif;?>
            <?php if(!empty($grid_2) && is_array($grid_2)):?>
                <div class="column-grid">
                <?php if(is_admin()):?>
                        <img src="<?php echo $grid_2['image']['url'];?>"/>
                    <?php else:?>
                        <picture>
                            <source
                                data-srcset="<?php echo $grid_2['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_2['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:400px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_2['image']['sizes']['square-large']; ?>.webp 1x, <?php echo $grid_2['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:768px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_2['image']['sizes']['square-large-retina']; ?>.webp 1x, <?php echo $grid_2['image']['sizes']['square']; ?>.webp 2x"
                                media="(max-width:5000px)"
                            >
                            <img
                                class="lazyload"
                                data-src="<?php echo $grid_2['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_2['image']['sizes']['square-large-retina']; ?>.webp 2x"
                                alt="<?php echo $grid_2['image']['title']; ?>">
                        </picture>
                    <?php endif;?>
                    <div class="col-grid-content">
                        <div class="col-title">
                            <?php echo $grid_2['title'];?>
                        </div>
                        <div class="short-desc">
                            <?php echo $grid_2['short_description'];?>
                        </div>
                        <?php $link = $grid_2['link'];
                        if($link):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="btn btn-link-arrow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php echo esc_html( $link_title ); ?>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="15" viewBox="0 0 18.5 15"><path id="Union_1" data-name="Union 1" d="M-6775.5-1444h-8.5v-3h8.5v-6l10,7.5-10,7.5Z" transform="translate(6784 1453)" fill="#EE2628"/></svg>
                                </span>
                            </a>
                        <?php endif;?>
                    </div>
                </div>
            <?php endif;?>
            <?php if(!empty($grid_3) && is_array($grid_3)):?>
                <div class="column-grid">
                <?php if(is_admin()):?>
                        <img src="<?php echo $grid_3['image']['url'];?>"/>
                    <?php else:?>
                        <picture>
                            <source
                                data-srcset="<?php echo $grid_3['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_3['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:400px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_3['image']['sizes']['square-large']; ?>.webp 1x, <?php echo $grid_3['image']['sizes']['square-large']; ?>.webp 2x"
                                media="(max-width:768px)"
                            >
                            <source
                                data-srcset="<?php echo $grid_3['image']['sizes']['square-large-retina']; ?>.webp 1x, <?php echo $grid_3['image']['sizes']['square']; ?>.webp 2x"
                                media="(max-width:5000px)"
                            >
                            <img
                                class="lazyload"
                                data-src="<?php echo $grid_3['image']['sizes']['square']; ?>.webp 1x, <?php echo $grid_3['image']['sizes']['square-large-retina']; ?>.webp 2x"
                                alt="<?php echo $grid_3['image']['title']; ?>">
                        </picture>
                    <?php endif;?>
                    <div class="col-grid-content">
                        <div class="col-title">
                            <?php echo $grid_3['title'];?>
                        </div>
                        <div class="short-desc">
                            <?php echo $grid_3['short_description'];?>
                        </div>
                        <?php $link = $grid_3['link'];
                        if($link):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="btn btn-link-arrow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php echo esc_html( $link_title ); ?>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="15" viewBox="0 0 18.5 15"><path id="Union_1" data-name="Union 1" d="M-6775.5-1444h-8.5v-3h8.5v-6l10,7.5-10,7.5Z" transform="translate(6784 1453)" fill="#EE2628"/></svg>
                                </span>
                            </a>
                        <?php endif;?>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>