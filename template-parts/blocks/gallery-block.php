<?php
$images = get_field('upload_images');
if(!empty($images) && is_array($images)):?>
<section class="gutenberg-block gutenberg-gallery-block">
    <div class="container half left">
        <div class="gallery-container">
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php foreach($images as $img):?>
                        <div class="swiper-slide">
                            <div class="img-box">
                                <?php if(is_admin()): ?>
                                    <a href="">
                                        <img src="<?php echo $img['sizes']['large']; ?>" alt="">
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo $img['sizes']['fullwidth']; ?>" data-fancybox="gallery" data-caption="<?php echo $img['caption']; ?>">
                                        <picture>
                                            <source
                                                    data-srcset="<?php echo $img['sizes']['fullwidth']; ?>.webp 1x, <?php echo $img['sizes']['fullwidth-retina']; ?>.webp 2x" type="img/webp"
                                            />
                                            <source
                                                data-srcset="<?php echo $img['sizes']['fullwidth']; ?> 1x, <?php echo $img['sizes']['fullwidth-retina']; ?> 2x" type="<?php echo $img['mime_type']; ?>"
                                            />
                                            <img
                                                class="lazyload"
                                                data-src="<?php echo $img['sizes']['fullwidth']; ?>"
                                                alt="<?php echo $img['title']; ?>">
                                        </picture>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;?>