<?php
    $profile_1 = get_field('profile_1');
    $profile_2 = get_field('profile_2');

    $get_title = get_field('heading');
    $title = explode(' ', $get_title);
    $heading = '<span>' . $title[0] . '</span> ' . Implode(" ", array_slice($title,1));
?>
<section class="gutenberg-block gutenberg-profile-block">
    <div class="container">
        <div class="heading">
            <?php echo $heading;?>
        </div>
        <div class="profiles-container">
            <?php if(!empty($profile_1)):?>
            <div class="profile">
                <?php if(!empty($profile_1['image'])):?>
                    <div class="profile-image">
                        <?php if(is_admin()):?>
                            <img src="<?php echo $profile_1['size']['square'];?>">
                        <?php else:?>
                        <picture>
                            <source
                                    data-srcset="<?php echo $profile_1['image']['sizes']['square']; ?>.webp 1x, <?php echo $profile_1['image']['sizes']['square-large']; ?>.webp 2x" type="img/webp"
                            />
                            <source
                                data-srcset="<?php echo $profile_1['image']['sizes']['square']; ?> 1x, <?php echo $profile_1['image']['sizes']['square-large']; ?> 2x" type="<?php echo $profile_1['image']['mime_type']; ?>"
                            />
                            <img
                                class="lazyload"
                                data-src="<?php echo $profile_1['image']['sizes']['square']; ?>"
                                alt="<?php echo $profile_1['image']['title']; ?>">
                        </picture>
                        <?php endif;?>
                    </div>
                <?php endif;?>
                <div class="name">
                    <?php echo $profile_1['name'];?>
                </div>
                <div class="title">
                    <?php echo $profile_1['title'];?>
                </div>
                <?php if($profile_1['linkedin_link']):
                    $link = $profile_1['linkedin_link']['url'];
                    ?>
                    <a class="" href="<?php echo esc_url( $link ); ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" width="29" height="27.667" viewBox="0 0 29 27.667">
                    <path id="linkedin" d="M26.377,0H2.623A2.565,2.565,0,0,0,0,2.5V25.164a2.565,2.565,0,0,0,2.623,2.5H26.377A2.565,2.565,0,0,0,29,25.164V2.5A2.565,2.565,0,0,0,26.377,0ZM8.974,23.889a.746.746,0,0,1-.763.728H4.961a.746.746,0,0,1-.763-.728v-13a.746.746,0,0,1,.763-.728H8.21a.746.746,0,0,1,.763.728ZM6.586,8.94A3.019,3.019,0,0,1,3.5,5.994,3.019,3.019,0,0,1,6.586,3.049,3.019,3.019,0,0,1,9.673,5.994,3.019,3.019,0,0,1,6.586,8.94ZM25.957,23.948a.686.686,0,0,1-.7.67H21.767a.686.686,0,0,1-.7-.67v-6.1c0-.909.28-3.985-2.491-3.985-2.149,0-2.585,2.105-2.673,3.05v7.031a.686.686,0,0,1-.7.67H11.827a.686.686,0,0,1-.7-.67V10.834a.686.686,0,0,1,.7-.67H15.2a.686.686,0,0,1,.7.67v1.134c.8-1.141,1.981-2.022,4.5-2.022,5.584,0,5.552,4.977,5.552,7.711v6.29Z" fill="#373737"/>
                    </svg>
                    Linkedin
                    </a>
                <?php endif;?>
            </div>
            <?php endif;?>
            <?php if(!empty($profile_2)):?>
            <div class="profile">
                <?php if(!empty($profile_2['image'])):?>
                    <div class="profile-image">
                        <?php if(is_admin()):?>
                            <img src="<?php echo $profile_2['size']['square'];?>">
                        <?php else:?>
                        <picture>
                            <source
                                    data-srcset="<?php echo $profile_2['image']['sizes']['square']; ?>.webp 1x, <?php echo $profile_2['image']['sizes']['square-large']; ?>.webp 2x" type="img/webp"
                            />
                            <source
                                data-srcset="<?php echo $profile_2['image']['sizes']['square']; ?> 1x, <?php echo $profile_2['image']['sizes']['square-large']; ?> 2x" type="<?php echo $profile_2['image']['mime_type']; ?>"
                            />
                            <img
                                class="lazyload"
                                data-src="<?php echo $profile_2['image']['sizes']['square']; ?>"
                                alt="<?php echo $profile_2['image']['title']; ?>">
                        </picture>
                        <?php endif;?>
                    </div>
                <?php endif;?>
                <div class="name">
                    <?php echo $profile_2['name'];?>
                </div>
                <div class="title">
                    <?php echo $profile_2['title'];?>
                </div>
                <?php if($profile_2['linkedin_link']):
                    $link = $profile_2['linkedin_link']['url'];
                    ?>
                    <a class="" href="<?php echo esc_url( $link ); ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" width="29" height="27.667" viewBox="0 0 29 27.667">
                    <path id="linkedin" d="M26.377,0H2.623A2.565,2.565,0,0,0,0,2.5V25.164a2.565,2.565,0,0,0,2.623,2.5H26.377A2.565,2.565,0,0,0,29,25.164V2.5A2.565,2.565,0,0,0,26.377,0ZM8.974,23.889a.746.746,0,0,1-.763.728H4.961a.746.746,0,0,1-.763-.728v-13a.746.746,0,0,1,.763-.728H8.21a.746.746,0,0,1,.763.728ZM6.586,8.94A3.019,3.019,0,0,1,3.5,5.994,3.019,3.019,0,0,1,6.586,3.049,3.019,3.019,0,0,1,9.673,5.994,3.019,3.019,0,0,1,6.586,8.94ZM25.957,23.948a.686.686,0,0,1-.7.67H21.767a.686.686,0,0,1-.7-.67v-6.1c0-.909.28-3.985-2.491-3.985-2.149,0-2.585,2.105-2.673,3.05v7.031a.686.686,0,0,1-.7.67H11.827a.686.686,0,0,1-.7-.67V10.834a.686.686,0,0,1,.7-.67H15.2a.686.686,0,0,1,.7.67v1.134c.8-1.141,1.981-2.022,4.5-2.022,5.584,0,5.552,4.977,5.552,7.711v6.29Z" fill="#373737"/>
                    </svg>
                    Linkedin
                    </a>
                <?php endif;?>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>