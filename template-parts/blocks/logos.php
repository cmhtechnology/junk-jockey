<?php
/**
 * Logos block template.
 *
 * @package JunkJockey
 */

$junkjockey_id = 'logos-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
	$junkjockey_id = $block['anchor'];
}
$junkjockey_class = 'gutenberg-block logos-block';
if ( ! empty( $block['className'] ) ) {
	$junkjockey_class .= ' ' . $block['className'];
}

?>
<section id="<?php echo esc_attr( $junkjockey_id ); ?>" class="<?php echo esc_attr( $junkjockey_class ); ?>">
	<div class="container">
		<div class="logo-row">
			<?php
			if ( have_rows( 'jj_logos' ) ) :
				?>
				<?php
				while ( have_rows( 'jj_logos' ) ) :
					the_row();
					$junkjockey_img  = get_sub_field( 'img' );
					$junkjockey_link = get_sub_field( 'link' );
					?>
					<div class="logo-container">
						<?php if ( $junkjockey_img ) : ?>
							<?php if ( $junkjockey_link ) : ?>
								<a
									class="link"
									href="<?php echo esc_url( $junkjockey_link['url'] ); ?>"
									target="<?php echo esc_attr( $junkjockey_link['target'] ? $junkjockey_link['target'] : '_self' ); ?>">
							<?php endif; ?>

							<?php echo wp_get_attachment_image( $junkjockey_img['id'], 'full', false, array( 'class' => 'lazyload' ) ); ?>

							<?php if ( $junkjockey_link ) : ?>
								</a>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
				<?php
			endif;
			?>
		</div>
	</div>
</section>
