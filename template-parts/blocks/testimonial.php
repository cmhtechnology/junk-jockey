<?php
$junkjockey_title  = get_field( 'jj_title' );
$junkjockey_link   = get_field( 'jj_link' );
$junkjockey_bg_img = get_field( 'jj_bg_img' );
$junkjockey_style  = '';
if ( ! empty( $junkjockey_bg_img ) ) {
	$junkjockey_style = 'style="background-image: url(' . $junkjockey_bg_img['url'] . ');"';
}
?>

<section class="gutenberg-block gutenberg-testimonial-block" <?php echo wp_kses_post( $junkjockey_style ); ?>>
	<div class="container">
		<div class="testimonial-row">
			<div class="col-title column">
				<h2 class="title fw-bold"><?php echo $junkjockey_title;?></h2>

				<?php if ( $junkjockey_link ) : ?>
					<a
						class="btn btn-rounded btn-red"
						href="<?php echo esc_url( $junkjockey_link['url'] ); ?>"
						target="<?php echo esc_attr( $junkjockey_link['target'] ? $junkjockey_link['target'] : '_self' ); ?>">
						<?php echo esc_html( $junkjockey_link['title'] ); ?>
					</a>
				<?php endif; ?>
			</div>

			<div class="col-testimonials column">
				<?php if ( have_rows( 'jj_testimonials' ) ) : ?>
					<div class="testimonials fw-light">
						<div class="swiper">
							<div class="swiper-wrapper">
								<?php
								while ( have_rows( 'jj_testimonials' ) ) :
									the_row();
									$junkjockey_review = get_sub_field( 'review' );
									$junkjockey_author = get_sub_field( 'author' );
									?>
									<div class="swiper-slide">
										<div class="testimonial">
											<?php if ( $junkjockey_review ) : ?>
												<div class="review"><?php echo esc_html( $junkjockey_review ); ?></div>
											<?php endif; ?>

											<?php if ( $junkjockey_author ) : ?>
												<span class="author text-italic"><?php echo esc_html( $junkjockey_author ); ?></span>
											<?php endif; ?>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
							<div class="swiper-pagination"></div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
