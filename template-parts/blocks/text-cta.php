<?php
    $text_banner = get_field('text_banner');
    $cta_link = get_field('cta_button');
if($text_banner):?>
<section class="gutenberg-block gutenberg-text-cta-block">
    <div class="container">
        <div class="text-content">
            <?php echo $text_banner;?>
        </div>
        <?php if($cta_link):
            $link_url = $cta_link['url'];
            $link_title = $cta_link['title'];
            $link_target = $cta_link['target'] ? $cta_link['target'] : '_self';
            ?>
            <a class="btn btn-rounded btn-red" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
        <?php endif;?>
    </div>
</section>
<?php endif;?>