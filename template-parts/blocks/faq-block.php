<section class="gutenberg-block gutenberg-faq-block">
    <div class="container">
        <div class="heading">
            <?php echo get_field('heading');?>
        </div>
        <?php $faqs = get_field('faqs');
        if($faqs):?>
        <div class="accordion-container">
            <?php foreach($faqs as $faq):?>
                <div class="accordion">
                    <div class="accordion-title">
                        <span><?php echo $faq['question'];?></span>
                    </div>
                    <div class="accordion-panel"><?php echo $faq['answer'];?></div>
                </div>
            <?php endforeach;?>
        </div>
        <?php endif;?>
    </div>
</section>