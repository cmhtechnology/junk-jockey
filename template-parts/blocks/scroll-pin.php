<?php $panels = get_field('jj_panel');
if(!empty($panels) && is_array($panels)):?>
<div class="gutenberg-block gutenberg-scroll-pin">
    <div class="bg-slide">
        <div class="container-bg">
            <div>

                <?php foreach($panels as $panel):
                    $image = $panel['bg_img'];
                ?>
                    <div class="slide-background bg-cover lazyload"
                         data-bgset="
                            <?php echo $image['sizes']['fullwidth']; ?>.webp 1x, <?php echo $image['sizes']['fullwidth-retina']; ?>.webp 2x
                         ">

                    </div>
                <?php endforeach;?>

            </div>
        </div>
    </div>

    <div class="container-slide container">
        <?php foreach($panels as $panel):
        $title = $panel['title'];
        $desc = $panel['desc'];
        $links = $panel['links'];
            ?>
            <div class="slide">
                <div class="slide-content">
                    <div class="title">
                        <?php echo $title;?>
                    </div>
                    <div class="desc">
                        <?php echo $desc;?>
                    </div>
                    <?php if(!empty($links) && is_array($links)):?>
                        <div class="links">
                            <?php foreach($links as $key => $link):
                                $link_url = $link['link']['url'];
                                $link_title = $link['link']['title'];
                                $link_target = $link['link']['target'] ? $link['link']['target'] : '_self';
                                ?>
                                <a class="btn btn-rounded btn-solid-arrow btn-solid-arrow__<?php echo $key === 0 ? 'red' : 'blue';?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                    <?php echo esc_html( $link_title ); ?>
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="15" viewBox="0 0 18.5 15"><path id="Union_1" data-name="Union 1" d="M-6775.5-1444h-8.5v-3h8.5v-6l10,7.5-10,7.5Z" transform="translate(6784 1453)" fill="#EE2628"/></svg>
                                    </span>
                                </a>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>
<?php endif;?>