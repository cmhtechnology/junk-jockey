<?php $slides = get_field('slides');
if(!empty($slides) && is_array($slides)):?>
<div class="gutenberg-block gutenberg-homepage-slider">
    
    <div class="homepage-slider">
        <div class="swiper">
            <div class="swiper-wrapper">
            <?php foreach($slides as $slide):
            $image = $slide['image'];
            $link = $slide['link_button'];

            // echo '<pre>';
            // echo var_dump($image);
            // echo '</pre>';
            ?>
                <div class="swiper-slide bg-cover lazyload"
                    data-bgset="
                    <?php echo $image['sizes']['fullwidth']; ?>.webp 1x, <?php echo $image['sizes']['fullwidth-retina']; ?>.webp 2x
                    "
                >
                    <div class="slide-content"
                        data-title="<?php echo $slide['title'];?>"
                        data-desc="<?php echo $slide['short_description'];?>"
                        data-link="<?php echo $link['url'];?>"
                        data-linktext="<?php echo $link['title'];?>"
                    >
                        <div class="container">
                            <div>
                                <h1><?php echo $slide['title'];?></h1>
                                <div class="slide-description"><?php echo $slide['short_description'];?></div>
                            </div>
                            <?php if($link):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <div>
                                <a class="btn btn-rounded border-white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
            </div>
        </div>
        <div class="swiper-nav-container">
            <div class="container">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>

</div>
<?php endif;?>