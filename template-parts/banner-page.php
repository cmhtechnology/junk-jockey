<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

$get_title = get_the_title();
$title = explode(' ', $get_title);
$heading = '<span>' . $title[0] . '</span> ' . Implode(" ", array_slice($title,1));
$image = get_field('banner_image');
?>
<div class="banner-page bg-cover lazyload"
     data-bgset="<?php echo $image['sizes']['fullwidth']; ?>.webp 1x, <?php echo $image['sizes']['fullwidth-retina']; ?>.webp 2x"
>
    <div class="container">
        <h1><?php echo $heading;?></h1>
    </div>
</div>