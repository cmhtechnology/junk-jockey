<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package JunkJockey
 */

?>
</main>

	<footer id="colophon" class="footer bg-light">
		<div class="container">
            <?php $site_logo = get_field('site_logo_footer', 'option');
                if($site_logo):?>
				<div class=footer-logo>
					<a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
						<img src="<?php echo $site_logo['url']; ?>" alt="<?php bloginfo('name'); ?> logo"/>
					</a>
				</div>
            <?php endif;?>

			<?php if ( has_nav_menu( 'menu-2' ) ) : ?>
				<nav class="footer-nav">
					<?php wp_nav_menu(
							array(
								'theme_location' => 'menu-2',
								'container'      => false,
								'fallback_cb'    => false,
								'depth'          => 1,
							)
						);
					?>
				</nav>
			<?php endif; ?>

			<?php
			$address = get_field( 'address', 'option' );
			$contact_phone_number = get_field( 'contact_phone_number', 'option' );
			$contact_email = get_field( 'contact_email', 'option' );
			if (
				$address ||
				$contact_phone_number ||
				$contact_email
			) :
				?>
				<div class="site-info">
					<span class="address">
						<svg id="icon_maps_place_24px" data-name="icon/maps/place_24px" xmlns="http://www.w3.org/2000/svg" width="29" height="32" viewBox="0 0 29 32">
						<path id="Boundary" d="M0,0H29V29H0Z" fill="none"/>
						<g id="_Color" data-name=" ↳Color" transform="translate(5 2)" fill="none" stroke-miterlimit="10">
							<path d="M11,30h0a68.745,68.745,0,0,1-5.5-6.8C2.991,19.653,0,14.568,0,10.5A10.2,10.2,0,0,1,3.22,3.073,11.2,11.2,0,0,1,11,0a11.2,11.2,0,0,1,7.78,3.073A10.2,10.2,0,0,1,22,10.5c0,4.068-2.991,9.153-5.5,12.7A68.745,68.745,0,0,1,11,30ZM11,6.75A3.847,3.847,0,0,0,7.071,10.5,3.847,3.847,0,0,0,11,14.251a3.847,3.847,0,0,0,3.929-3.75A3.847,3.847,0,0,0,11,6.75Z" stroke="none"/>
							<path d="M 11.00000190734863 26.99440956115723 C 12.01515197753906 25.81125068664551 13.43841552734375 24.06962203979492 14.86682987213135 22.04877090454102 C 18.17700004577637 17.36565971374512 20 13.26436996459961 20 10.50041007995605 C 20 8.244680404663086 19.07645988464355 6.120790004730225 17.39949035644531 4.519989967346191 C 15.69713020324707 2.894949913024902 13.42442035675049 2 11 2 C 8.575579643249512 2 6.30286979675293 2.894949913024902 4.600510120391846 4.519989967346191 C 2.923549890518188 6.120800018310547 2 8.244680404663086 2 10.50041007995605 C 2 13.26436996459961 3.822999954223633 17.36565971374512 7.133170127868652 22.04875946044922 C 8.56158447265625 24.06961631774902 9.984810829162598 25.81120681762695 11.00000190734863 26.99440956115723 M 11 4.750070095062256 C 14.26912975311279 4.750070095062256 16.92877006530762 7.329659938812256 16.92877006530762 10.50041007995605 C 16.92877006530762 13.6711597442627 14.26912975311279 16.2507495880127 11 16.2507495880127 C 7.730869770050049 16.2507495880127 5.071229934692383 13.6711597442627 5.071229934692383 10.50041007995605 C 5.071229934692383 7.329659938812256 7.730869770050049 4.750070095062256 11 4.750070095062256 M 11 30 L 10.99884033203125 29.99876976013184 C 10.88560009002686 29.87874031066895 8.194609642028809 27.01542091369629 5.499969959259033 23.20317077636719 C 2.990910053253174 19.65345001220703 -1.77635683940025e-15 14.56816959381104 -1.77635683940025e-15 10.50041007995605 C -1.77635683940025e-15 7.692820072174072 1.14339005947113 5.055160045623779 3.21953010559082 3.073309898376465 C 5.295670032501221 1.091459989547729 8.058830261230469 -1.77635683940025e-15 11 -1.77635683940025e-15 C 13.94116973876953 -1.77635683940025e-15 16.70433044433594 1.091459989547729 18.78046989440918 3.073309898376465 C 20.85662078857422 5.055160045623779 22 7.692820072174072 22 10.50041007995605 C 22 14.56816959381104 19.00909042358398 19.65345001220703 16.50003051757812 23.20317077636719 C 13.80539035797119 27.01542091369629 11.11439990997314 29.87874031066895 11.00115966796875 29.99876976013184 L 11 30 Z M 11 6.750070095062256 C 8.833669662475586 6.750070095062256 7.071229934692383 8.432470321655273 7.071229934692383 10.50041007995605 C 7.071229934692383 12.56834983825684 8.833669662475586 14.2507495880127 11 14.2507495880127 C 13.16631984710693 14.2507495880127 14.92877006530762 12.56834983825684 14.92877006530762 10.50041007995605 C 14.92877006530762 8.432470321655273 13.16631984710693 6.750070095062256 11 6.750070095062256 Z" stroke="none" fill="#717171"/>
						</g>
						</svg>
						<?php echo $address; ?>
					</span>
					<span class="phone">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35" height="35" viewBox="0 0 35 35">
							<image id="icons8-phone-50" width="35" height="35" opacity="0.54" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAADk0lEQVRoge3ZTYhVdRjH8c9NHaUoh3JKU4tGcCU04SItWjSImzCIqCCJaSQxyGoZraVVhVH0ItSmdhWNVvSykKJFbwNOQUTaIouKIB2zF5xe7LR4zvB3hnvu3HM692UxXzibe37/c37n3P/z/J//c1hkkZ5yMyYxg8+xtbd2yjOAp5HNO05hbQ99lWIlDgvjZ7AXq3Ao/+2t3llrn9X4TBj+AZvnnTuRn9vVfWvtcxGOCKNf4sommp35+V9wRfestc9yfCBMfoFLWmgn9PEUe0SY+xbrFtCuxslcP9ZhX6WZFsZubFN/V66fxppOmarCjDA2WGLMm/mYiY44qsjXwtSmEmPWiaDPcEcnTFVhNoB3lhy3Ox93tHZHFdkrDL1YctyqfNw/aNRtqgob8C9+xYUlxt0tHmSyE6aqMluW3NumfgWO68M0fLs035e2oX8410/hvA76Ks0SUZpk2LOA9lKczrXbOuyrErcKcz/i/Ba6A7nuUDdMVaGBT4TJRws0K/BbrrmnS74qcQ3+wlncUKB5QNpoXdUlX5XYJwV+synWwBu55tMCTV8wIPbomdjyNmNQSg4HRbLoS0bEVjfDeIFmA37ONQf0yerejHFp735tgWarFPz7u+SrEk9KKbmogzIq/XuPd8lXaZbhPWHyQ8WBvUNku0zsOPuSIXwjTL4tkkEzbpMeZr8+jZmN+EmYfFlxltohTbPnWuh6ytViEczwvOI3PiolgIOaT8clIplMicX3mChcu8Z1+N3Cgb1FSs2T5vbJtknr1PzjVVxWu+sCtuNP6WGK/plhadGcFlvj2YogE3E3Jnaau6WK+gTu7Jz9udwiPcwLimNhEK+b+9ZP4yFRgJ7LepFMZnUToo/WcbZL0+wVxdmsgftF3Dwr9jOt2CXF4knRR+s4159z03e0LiCXl7juWql3lokpeXlFj20zIqXmj9TbfRyTOqGnFJdKtbFRakZ8X/MN10hJYqrG6xYyhPfzG84orpqrcHF+3T9qvGZLluEpaW4/o56N14NSvddVxqXm+DGRFKqyRUr1N/1/a+UZkVbws3gMF5S8xrCUSJ6o1V1JBkQP4G9pX7NHe03AIXyVj3tXTNuesxkfS7FzVLRni3rN66Xy5kgLXU9oiCbg7FvOROP8JfFJY5P4PD6K76R027UisixLxcehw+IrQLMqOBOpfGWPPJZmGPfhNZHdzojqd59yJc0ii9TJf3jSBy2AlUehAAAAAElFTkSuQmCC"/>
						</svg>
						<a href="tel:<?php echo $contact_phone_number; ?>"><?php echo $contact_phone_number; ?></a>
					</span>
					<span class="email">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="39" height="39" viewBox="0 0 39 39">
							<image id="icons8-email-64" width="39" height="39" opacity="0.87" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAD9ElEQVR4nO3aW4xdUxzH8c90arTTRgdpValbpCEuESniVs2gpAjRoB4aDQlJExJP8yASj4KEiAckxC0RVNRDK65Jq4IQgrqWiIhbKCVKL2PGw3+fnjMz55zZe5+zzxizv8nJ2Wfttf7nv357r/9a/7U3JSUlJSUlJSUlJSVTka6a45lYgd4J8qVT/IVn8ffoEysxPEU+Kyudnl4jwIya4xdxI/5pquXkoRv3YVnye0a9SquFOk8k30+jpxPeFUwPnhF9ejz5Xl2vYkWAIzCAIbyK2R1xsxh68YLoy4DoWyoB4AYxBN7GgUV7WgB92Cz6cH1SlkkAIljsxhYsKMrTAjgI72MPrqkpzywALBdTx1c4qt2eFsBh+Bw7cdmoc2MEmJbC4AZcIIbBZhzfFjeL4Rjh4wJchHXjNUgjALyOfjFtbsRpOR0skpOxCbNwngjg45JWAHgPS7ADr+DcjA4Wydl4DYM4RwTuVGQRAD7DWfge640dYxPBcrFw2yZ825LXUKMgWI9GUbbTZJ2lcs0CjajMs0O4KWWbdpJnndJWARi70uoUeVeqbReAWGs/lbS93cgUuwhuS/5rnQZJTRMKEYDIth5M2t8ve3BNQxfuSf7jUSMz2bQUJkDFwTsSG09in5x26jEdjyS275X/LitUgAoDiZ31YpepVfbFc6pDrBU6IgCsEUFqI+a0YGdOYmNY3F2tkisXyMNCcZsuESu0uTlszE3anoGfcB0Wt8vBerTjDujC3YmdB3Cp2Hz8VIiSloPxkcjoVuBwfIntOL0F/wodAt142NhAtRS/4xssSmHnSNHZP3F+TflCbE3K+3P6WJgAPVircaBajJ/xI05qYuc4fIdf1b/S88Wyd4fI+LJSiACzRDIyhJub1DsW3+I3nFnn/Kn4BT/gxCZ25uFDMTwuyehr2wXowxsiDb02pQNbxRW8sKa8H3/gaxydws7+eAe7ZMtI2yrAPJER7sIVGZyYjw+SdleqBsqPcUgGO30iEdotAmUa2iZAZd9t9JVMywF4S9w5g3gzKctKX2JnD65KUb8tAiwSEX272InJy2w8L5KaVp497Ce27Aaxapy6LQtQidLb/Lf2BXvxsgjEa5rUa0mAU1Sj9Al5PS2QmaqzUaMNmtwCLJUtSk8UPWJINZqScwlwsYjSn+DQdnlaID3i+f8wbh11LrMAV4tp5l35EpqJolv1SXDtyjTXw9FNItJONrpVN1EqIqQWoLLpuMHkfmVmGh4S/bpLSgEe8/96QWKa2KccVo0NewWot7G4Ci/hFpMj6KXhTrEEv3z0iVoBdtYcL8MXBTs1kezt61R9TW6tkRe8pKSkpKSkpKSkpGSK8S8myFd3dQX6pwAAAABJRU5ErkJggg=="/>
						</svg>
						<a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a>
					</span>
				</div>
			<?php endif; ?>

			<?php $social_media = get_field('social_media', 'option');
			if(!empty($social_media) && is_array($social_media)):?>
				<div class="social-media-container">
					<?php foreach($social_media as $sm):?>
						<div class="s-media">
							<a href="<?php echo $sm['link']; ?>" target="_blank">
								<img src="<?php echo $sm['icon']['url']; ?>"/>
							</a>
						</div>
					<?php endforeach;?>				
				</div>
			<?php endif;?>

			<div class="copyright">
				&copy; <?php echo esc_html( date( 'Y' ) ); ?> <?php bloginfo( 'name' ); ?>. All rights reserved. Site build by <a href="https://www.cmhtechnology.com/" target="_blank">CMH Technology</a>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
